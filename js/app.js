$(document).foundation();

(function($) {

	// jQuery
	$('#toggle').click(function(){
		$('nav ul').slideToggle();
	});

	$( window ).resize(function() {
		if ( $( window ).width() > 639 ) {
			$('nav ul').show();	
		}
	});

})(jQuery);